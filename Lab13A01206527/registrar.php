<?php
    session_start();
    
    if (isset($_SESSION["user"])) {
        include("_header.html");
        include("_inicia.html");
        include("_footer.html");
        
    } else if($_POST["edad"] < 18) {
        $_SESSION["error_edad"] = '<span class="red">Debes tener más de 18 años</span>'; 
        $_SESSION["edad"] = $_POST["edad"];
        echo '<script type="text/javascript">alert("Debes ser mayor de 18 años");</script>';   
        include("index.php");
    
    }elseif($_POST["pass1"]!=$_POST["pass2"]){
        echo '<script type="text/javascript">alert("La contraseña no coincide");</script>';   
        include("index.php");
        
    } else if(isset($_POST["nombre"]) && isset($_POST["apellido"]) && isset($_POST["edad"]) && isset($_POST["mail"]) && isset($_POST["pass1"]) && isset($_POST["pass2"])) {
        
        $_SESSION["user"] = $_POST["nombre"];
        $_SESSION["apellido"] = $_POST["apellido"];
        $_SESSION["edad"] = $_POST["edad"];
        $_SESSION["mail"] = $_POST["mail"];
        $_SESSION["pass1"]=$_POST["pass1"];
        $_SESSION["pass2"]=$_POST["pass2"];
        
        
        $target_dir = "media/";
        $target_file = $target_dir . basename($_FILES["foto"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["foto"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["foto"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["foto"]["name"]). " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        
        $_SESSION["foto"] = $target_file;
        
        $_SESSION["inicia"] = true;
        
        
        include("_header.html");
        include("_inicia.html");
        include("_footer.html");
    
    } else {
        
        header("location:index.php");
    }
?>
