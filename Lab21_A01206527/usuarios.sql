-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 24, 2017 at 12:56 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `usuarios`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `crearUsuarios` (IN `nombre` VARCHAR(50), IN `edad` INT(2), IN `telefono` BIGINT(11), IN `mail` VARCHAR(40), IN `empresa` VARCHAR(100))  begin

INSERT INTO usuarios (`nombre`, `edad`, `telefono`, `mail`, `empresa`) VALUES (nombre,edad,telefono,mail,empresa);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `cursos`
--

CREATE TABLE `cursos` (
  `idCurso` int(11) NOT NULL,
  `nombreCurso` varchar(100) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `nota` varchar(250) NOT NULL,
  `objetivos` varchar(250) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cursos`
--

INSERT INTO `cursos` (`idCurso`, `nombreCurso`, `ubicacion`, `descripcion`, `nota`, `objetivos`, `precio`) VALUES
(1, 'Liderazgo', 'ITESM, Querétaro', 'Curso acerca del liderazgo empresarial.', 'No llevar comida.', 'Aprneder acerca del liderazo empresarial', 100),
(2, 'Negocios Internacionales', 'ITESM, Qro', 'Aprender un poco acerca de los negocios internacionales.', 'No se admiten mascotas. ', 'Aprender negocios internacionales.', 200),
(3, 'Proactividad', 'ITESM, Qro', 'Curso acerca de la proactividad en el trabajo', 'No menores de edad', 'Aprender acerca de la proactividad', 50),
(4, 'Organización Empresarial', 'UVM Qro', 'Entiende como está organizada un empresa.\r\n', 'No llevar alimentos.', 'Aprender organización emprresarial', 200),
(5, 'Etica empresarial', 'ITESM, Mty\r\n', 'Aprender acerca de cómo la ética impacta nuestro entorno.', 'Llevar buena actitud.', 'Aprender a cómo impacta la ética en la empresa.', 100),
(6, 'Prueba', '', '', '', 'Segunda Prueba', 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `edad` int(2) NOT NULL,
  `telefono` bigint(11) NOT NULL,
  `mail` varchar(40) NOT NULL,
  `empresa` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `edad`, `telefono`, `mail`, `empresa`) VALUES
(1, 'Brenda Flores Parada', 10, 4422738492, 'brenda_fparada@hotmail.com', 'Microsoft'),
(2, 'Pablo Pozos Aguilar', 10, 4423748293, 'pabloPozos@hotmail.com', 'Microsoft'),
(3, 'Carlos Becerra Ortiz', 32, 4423748329, 'carlosBecerra@hotmail.com', 'Google'),
(4, 'Rafael Solis Utrilla', 23, 4423492034, 'rafaelSolis@hotmail.com', 'Oracle'),
(10, 'Pedro Torres Serna', 20, 4423849123, 'pedroTorres@hotmail.com', 'General Electric'),
(17, 'Jose Silva Alcantar', 20, 4424859340, 'josesilva@hotmail.com', 'Mars'),
(18, 'Diego Lopez Fernandez', 18, 4427839273, 'diegolopez@hotmail.com', 'Gerber'),
(19, 'prueba', 21, 44223948503, 'pruebaq@hotmail.com', 'prueba');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`idCurso`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cursos`
--
ALTER TABLE `cursos`
  MODIFY `idCurso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
