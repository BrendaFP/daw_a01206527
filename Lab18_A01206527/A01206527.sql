-------La suma de las cantidades e importe total de todas las entregas realizadas durante el 97. 
SET DATEFORMAT DMY
SELECT SUM(E.Cantidad) AS TotalCantidad, SUM(M.Costo*E.Cantidad) AS TotalCosto 
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave 
AND Fecha BETWEEN '1/01/1997' AND '31/12/1997'

-------Para cada proveedor, obtener la raz�n social del proveedor, n�mero de entregas e importe total de las entregas realizadas. 
SELECT Pr.RazonSocial, COUNT(E.RFC) AS NoEntregas, SUM(M.Costo*E.Cantidad) AS ImporteTotal
FROM Entregan E, Materiales M, Proveedores Pr
WHERE M.Clave=E.Clave AND Pr.RFC=E.RFC
GROUP BY Pr.RazonSocial

-------Por cada material obtener la clave y descripci�n del material, la cantidad total entregada, 
-------la m�nima cantidad entregada, la m�xima cantidad entregada, el importe total de las entregas 
-------de aquellos materiales en los que la cantidad promedio entregada sea mayor a 400. 
SELECT M.Clave, M.Descripcion, SUM(E.Cantidad) AS CantidadTotal, MIN(E.Cantidad) AS EntregaMinima, MAX(E.Cantidad) AS EntregaMaxima, SUM (M.Costo*E.Cantidad) AS ImporteCantidad 		
FROM Entregan E, Materiales M
WHERE E.Clave=M.Clave
GROUP BY M.Clave, M.Descripcion
HAVING AVG(E.Cantidad)>400

------Para cada proveedor, indicar su raz�n social y mostrar la cantidad promedio de cada material entregado, 
------detallando la clave y descripci�n del material, excluyendo aquellos proveedores para los que la cantidad 
------promedio sea menor a 500. 
SELECT P.RazonSocial, AVG(E.Cantidad) AS PromedioMaterial, M.Clave, M.Descripcion
FROM Proveedores P, Entregan E, Materiales M
WHERE E.Clave=M.Clave AND P.RFC=E.RFC
GROUP BY P.RazonSocial, M.Clave, M.Descripcion
HAVING AVG(E.Cantidad)<500

------Mostrar en una solo consulta los mismos datos que en la consulta anterior pero para dos grupos de proveedores:
------aquellos para los que la cantidad promedio entregada es menor a 370 y aquellos para los que la cantidad 
------promedio entregada sea mayor a 450.
SELECT P.RazonSocial, AVG(E.Cantidad) AS PromedioMaterial, M.Clave, M.Descripcion
FROM Proveedores P, Entregan E, Materiales M
WHERE E.Clave=M.Clave AND P.RFC=E.RFC
GROUP BY P.RazonSocial, M.Clave, M.Descripcion
HAVING AVG(E.Cantidad) < 370 OR AVG(E.Cantidad) > 450

-------INSERT INTO tabla VALUES (valorcolumna1, valorcolumna2, [...] , valorcolumnan) ; 

SELECT * FROM Materiales

INSERT INTO Materiales VALUES (1500,'Martillo',70,2.00)
INSERT INTO Materiales VALUES (1510,'Clavo',10,2.00)
INSERT INTO Materiales VALUES (1520,'Desarmador',2,2)
INSERT INTO Materiales VALUES (1530,'Pala',350,2.25)
INSERT INTO Materiales VALUES (1540,'Esp�tula',100,2.5)

------ Clave y descripci�n de los materiales que nunca han sido entregados. 
SELECT M.Clave, M.Descripcion
FROM Materiales M
WHERE Clave NOT IN (
			SELECT Clave 
			FROM entregan
			)

------Raz�n social de los proveedores que han realizado entregas tanto 
------al proyecto 'Vamos M�xico' como al proyecto 'Quer�taro Limpio'. 
SELECT RazonSocial 
FROM Proveedores
WHERE RFC IN (SELECT E.RFC FROM Entregan E, Proyectos P
			  WHERE E.Numero = P.Numero 
			  AND (P.Denominacion LIKE 'Vamos Mexico'
			  OR P.Denominacion LIKE 'Queretaro Limpio'))

------Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio 
------de cantidad entregada es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'. 
SELECT Descripcion  
FROM Materiales
WHERE Clave NOT IN (SELECT E.Clave 
					FROM Entregan E, Proyectos P
					WHERE E.Numero = P.Numero 
					AND P.Denominacion LIKE 'CIT Yucatan')

------Raz�n social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada es mayor 
------al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'. 
SELECT Pr.RazonSocial, AVG(E.Cantidad) AS PromedioCantidad 
FROM Proveedores Pr, Entregan E
WHERE Pr.RFC = E.RFC
GROUP BY Pr.RazonSocial
HAVING AVG(E.Cantidad) > (SELECT AVG(Cantidad) 
						  FROM Entregan WHERE RFC LIKE 'VAGO780901' 
						  GROUP BY RFC)

-------RFC, raz�n social de los proveedores que participaron en el proyecto 'Infonavit Durango' y cuyas cantidades 
-------totales entregadas en el 2000 fueron mayores a las cantidades totales entregadas en el 2001.
CREATE VIEW CantidadesEntregadas2000(RFC, Cantidad) AS (
	SELECT RFC, SUM(Cantidad) 
	FROM Entregan 
	WHERE Fecha between '01/01/2000' and '31/12/2000' 
	GROUP BY RFC
)

CREATE VIEW CantidadesEntregadas2001(RFC, Cantidad) AS (
	SELECT RFC, SUM(Cantidad) 
	FROM Entregan 
	WHERE Fecha BETWEEN '01/01/2001' and '31/12/2001' 
	GROUP BY RFC
)

SELECT E.RFC AS RFC, Pr.RazonSocial 
FROM Entregan E, Proveedores Pr, Proyectos P, CantidadesEntregadas2000 C2000, CantidadesEntregadas2001 C2001
WHERE Pr.RFC = E.RFC AND P.Numero = E.Numero AND P.Denominacion LIKE 'Infonavit Durango' AND 
E.Fecha BETWEEN '01/01/2000' AND '31/12/2000' AND C2000.RFC = C2001.RFC and C2000.Cantidad > C2001.Cantidad
GROUP BY E.RFC, Pr.RazonSocial
