<?php
    function connectDb(){
      $servername= "localhost";
      $username = "root";
      $password = "";
      $dbname = "usuarios";
    
      $mysql = mysqli_connect($servername, $username, $password, $dbname);
    
      // Check connection
      if (!$mysql) {
          die("Connection failed: " . mysqli_connect_error());
      }
      return $mysql;
    }
    
    function closeDB($mysql){
      mysqli_close($mysql);
    
    }

    function getUsuarioTable() {
        $db = connectDb();
        $query='SELECT * FROM usuarios';
         // Query execution; returns identifier of the result group
        $result = $db->query($query);
        $cards = " ";
         // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
         $cards .= '
         <br>
         <div class="col s12 m7">
             <h2>'.$fila["nombre"].'</h2>
             <div class="card horizontal">
               <div class="card-stacked">
                 <div class="card-content">
                     <p>'.$fila["edad"].'</p>
                     <p>'.$fila["telefono"].'</p>
                 </div>
                 <div class="card-action">
                   <a href="#">'.$fila["mail"].'</a>
                   <br>
                 </div>
               </div>
             </div>
           </div>';
    
        }
        mysqli_free_result($result);
        closeDb($db);
        return $cards;
    }
    
    
function getUsuario($idUsuario){
        $db = connectDb();
        //Specification of the SQL query
        $query='SELECT * FROM usuarios WHERE idUsuario="'.$idUsuario.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        
        $cards='<div class="row">
        <div class="col s12 m6">
          <div class="card blue-grey darken-1">';

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
         $cards .= '
         <br>
         <div class="col s12 m7">
             <h2>'.$fila["nombre"].'</h2>
             <div class="card horizontal">
               <div class="card-stacked">
                 <div class="card-content">
                     <p>'.$fila["edad"].'</p>
                     <p>'.$fila["telefono"].'</p>
                 </div>
                 <div class="card-action">
                   <a href="#">'.$fila["mail"].'</a>
                   <br>
                   <a href="editar.php?idUsuario='.$fila["idUsuario"].'">Editar</a>
                   <a href="eliminar.php?idUsuario='.$fila["idUsuario"].'">Eliminar</a>
                 </div>
               </div>
             </div>
           </div>';
    
        }
        mysqli_free_result($result);
        closeDb($db);
        return $cards;
    }



function getName($idUsuario){
        $db = connectDb();
        //Specification of the SQL query
        $query='SELECT nombre FROM usuarios WHERE idEvento="'.$idUsuario.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        
        $cards='';

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
         $cards .= '
         <br>
         <div class="col s12 m7">
             <h2>'.$fila["nombre"].'</h2>
             
           </div>';
    
        }
        mysqli_free_result($result);
        closeDb($db);
        return $cards;
    }
?>