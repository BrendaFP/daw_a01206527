<?php
    session_start();
    require_once("util.php");
    include("_header.html");
    include("_agregar.html");
    echo "<h1>Todos los usuarios</h1>";
    echo getUsuario();
    echo "<h1>Telefonos de cada usuario</h1>";
    echo getNombreTel();
    echo "<h1>Usuarios con mas de 18 años</h1>";
    echo getEdad();
    
    echo "<h1>Preguntas</h1>";
    echo "<h3>¿Por qué es una buena práctica separar el modelo del controlador?</h3>";
    echo "<ul> Porque te permite tener un mejor control del código. En un futuro, el código permite ser escalable. Proporciona una mejor estructura  </ul>";
    echo "<h3>¿Qué es SQL injection y cómo se puede prevenir?</h3>";
    echo "<ul>SQL Injection es una vulnerabilidad que permite a un atacante realizar consultas a una base de datos, se vale de un incorrecto filtrado de la información que se pasa a través de los campos y/o variables que usa un sitio web, es por lo general usada para extraer credenciales y realizar accesos ilegítimos, práctica un tanto neófita, ya que un fallo de este tipo puede llegar a permitir ejecución de comandos en el servidor, subida y lectura de archivos, o peor aún, la alteración total de los datos almacenados.</ul>";
    echo "<ul>Se puede prevenir al:</ul>";
    echo "<ul>a)Escapar los caracteres especiales utilizados en las consultas SQL</ul>";
    echo "<ul>b)Delimitar los valores de las consultas</ul>";
    echo "<ul>c)Verificar siempre los datos que introduce el usuario</ul>";
    echo "<ul>d)Asignar mínimos privilegios al usuario que conectará con la base de datos</ul>";
    
    include("_footer.html");
    include("_footer.html");
    if (isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        include("_mensaje.html");
        unset($_SESSION["mensaje"]);
    }
      
?>