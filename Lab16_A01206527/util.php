<?php
    function connectDb(){
      $servername= "localhost";
      $username = "root";
      $password = "root";
      $dbname = "usuarios";
    
      $mysql = mysqli_connect($servername, $username, $password, $dbname);
    
      // Check connection
      if (!$mysql) {
          die("Connection failed: " . mysqli_connect_error());
      }
      return $mysql;
    }
    
    function closeDB($mysql){
      mysqli_close($mysql);
    
    }

    function getUsuario() {
        $db = connectDb();
        $query='SELECT * FROM usuarios';
         // Query execution; returns identifier of the result group
        $result = $db->query($query);
        $cards = " ";
         // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
         $cards .= '
         <br>
         <div class="col s12 m7">
             <h2>'.$fila["nombre"].'</h2>
             <div class="card horizontal">
               <div class="card-stacked">
                 <div class="card-content">
                     <p>'.$fila["edad"].'</p>
                     <p>'.$fila["telefono"].'</p>
                 </div>
                 <div class="card-action">
                   <a href="#">'.$fila["mail"].'</a>
                   <br>
                   <a href="editar.php?idUsuario='.$fila["idUsuario"].'">Editar</a>
                   <a href="eliminar.php?idUsuario='.$fila["idUsuario"].'">Eliminar</a>
                 </div>
               </div>
             </div>
           </div>';
    
        }
        mysqli_free_result($result);
        closeDb($db);
        return $cards;
    }

    function getEdad(){
        $db = connectDb();
        $sql = "SELECT Nombre, Edad, Telefono, Mail FROM usuarios WHERE Edad > 18";
        $result = $db->query($sql);
        $table = "
        <table border = '2px'>
            <thread>
                <tr>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Telefono</th>
                    <th>Correo</th>
    
                </tr>
            </thread>
            <tbody>";
    
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
    
            $table .= '
              <tr>
                  <td>'.$row["Nombre"].'</td>
                  <td>'.$row["Edad"].'</td>
                  <td>'.$row["Telefono"].'</td>
                  <td>'.$row["Mail"].'</td>
              </tr>';
        }
    
        mysqli_free_result($result);
        closeDb($db);
        $table .= "</tbody></table>";
        return $table;
    }

    function getNombreTel(){
        $db = connectDb();
        $sql = "SELECT Nombre,Telefono FROM usuarios ";
        $result = $db->query($sql);
        $table = "
        <table border = '2px'>
            <thread>
                <tr>
                    <th>Nombre</th>
                    <th>Telefono</th>
                </tr>
            </thread>
            <tbody>";
    
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
    
            $table .= '
              <tr>
                  <td>'.$row["Nombre"].'</td>
                 
                  <td>'.$row["Telefono"].'</td>
               
              </tr>';
        }
    
        mysqli_free_result($result);
        closeDb($db);
        $table .= "</tbody></table>";
        return $table;
    }

    function getRegistro($db, $id){
        //Specification of the SQL query
        $query='SELECT * FROM usuarios WHERE idUsuario=$id';
         // Query execution; returns identifier of the result group
        $result= $db->query($query);   
        $fila = mysqli_fetch_array($result, MYSQLI_BOTH);
        return $fila;
    }


    function guardarRegistro($Nombre, $Edad, $Telefono, $Mail){
        $db = connectDb();
        
        // insert command specification 
        $query='INSERT INTO usuarios (`Nombre`, `Edad` ,`Telefono`,`Mail`) VALUES (?,?,?,?) ';
        // Preparing the statement 
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("ssss", $Nombre, $Edad, $Telefono,$Mail)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
         } 
    
        
         closeDb($db);
    }

    function editarRegistro($idUsuario, $nombre, $edad, $telefono, $mail){
        $db =  connectDb();
        // insert command specification 
        $query='UPDATE usuarios SET nombre=?, edad=?, telefono=?, mail=? WHERE idUsuario=?';
        // Preparing the statement 
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("sssss", $nombre, $edad, $telefono, $mail, $idUsuario)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
        }
        // update execution
        if ($statement->execute()) {
            echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
        closeDb($db);
    }

    function eliminarRegistro($id){
        $db =  connectDB();
        
        // insert command specification 
        $query='DELETE FROM usuarios WHERE idUsuario = ?';
        
        // Preparing the statement 
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("s", $id)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
            }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        closeDb($db);
    }
?>