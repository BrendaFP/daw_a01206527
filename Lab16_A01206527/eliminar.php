
<?php
    session_start();
    require_once("util.php");
    
    eliminarRegistro($_GET["idUsuario"]);
    
    $_SESSION["mensaje"] = 'El usuario fue eliminado exitosamente.';

    header("location:index.php");
?>