<?php
    session_start();
    require_once("util.php");
    include("_header.html");
    include("_agregar.html");
    echo "<h1>Todos los usuarios</h1>";
    echo getUsuario();
    echo "<h1>Telefonos de cada usuario</h1>";
    echo getNombreTel();
    echo "<h1>Usuarios con mas de 18 años</h1>";
    echo getEdad();
    
    echo "<h1>Preguntas</h1>";
    echo "<h3>¿Qué importancia tiene AJAX en el desarrollo de RIA's (Rich Internet Applications)?</h3>";
    echo "<ul> De esta forma no es necesario recargar la página para obtener respuesta, las operaciones se pueden realizar en tiempo real. El usuario no percibe que hay demoras en la página. Las comunicaciones se dan en segundo plano y no hay interrupciones </ul>";
    echo "<h3>¿Qué implicaciones de seguridad tiene AJAX? ¿Dónde se deben hacer las validaciones de seguridad, del lado del cliente o del lado del servidor?</h3>";
    echo "<ul>Las aplicaciones AJAX tienen una superficie de ataque mayor que las aplicaciones web convencionales. Las aplicaciones AJAX son más complicadas porque el procesamiento se realiza tanto en el lado del cliente como en el lado del servidor, sin embargo lo más seguro es que las validaciones se hagan del lado del servidor </ul>";
    echo "<h3>¿Qué es JSON?</h3>";
    echo "<ul>JSON (JAVASCRIPT OBJECT NOTATION) Es un formato para el intercambio de datos. JSON describe los datos con una sintaxis que se usa para identificar y gestionar datos. Puede ser usado para el intercambio de información entre distintas tecnologías.</ul>";
    
    
    include("_footer.html");
    include("_footer.html");
    if (isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        include("_mensaje.html");
        unset($_SESSION["mensaje"]);
    }
      
?>