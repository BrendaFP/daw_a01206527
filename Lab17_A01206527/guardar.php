
<?php
    session_start();
    require_once("util.php");
    if(isset($_POST["idUsuario"])) {
        editarRegistro($_POST["idUsuario"], $_POST["nombre"], $_POST["edad"], $_POST["telefono"], $_POST["mail"], $_POST["empresa"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se actualizó correctamente';
    } else {
        guardarRegistro( $_POST["nombre"], $_POST["edad"], $_POST["telefono"],$_POST["mail"], $_POST["empresa"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se registró correctamente';
    }
    header("location:index.php");
?>


