function validar(){
    var password1= document.getElementById("pass1");
    var password2= document.getElementById("pass2");


    if ( password1.value === password2.value ){
        alert("Usuario Valido");
    }
    else {
        alert("Usuario invalido");
    }
}

function agregar(){
    var p1=parseInt(document.getElementById("producto1").value);
    var p2=parseInt(document.getElementById("producto2").value);
    var p3=parseInt(document.getElementById("producto3").value);
    
    var total=0;
    var iva=0;
    var sub=0;
    
    sub+=(p1*22);
    sub+=(p2*17);
    sub+=(p3*14);
    
    iva=sub*.16;
    
    total=iva+sub;
    
    document.getElementById("subtotal").innerHTML="Subtotal: $"+sub;
    document.getElementById("iva").innerHTML="IVA: $"+iva;
    document.getElementById("total").innerHTML="Total: $"+ total;
    
}


function validarEntero(valor) {
    valor = parseInt(valor)

    if (isNaN(valor)) {
      return ""
    } else {
      return valor
    }
  }

  function valida_envia() {
    if (document.fvalida.nombre.value.length == 0) {
      alert("Tiene que escribir su nombre")
      document.fvalida.nombre.focus()
      return 0;
    }

    edad = document.fvalida.edad.value
    edad = validarEntero(edad)
    document.fvalida.edad.value = edad
    if (edad == "") {
      alert("Tiene que introducir un número entero en su edad.")
      document.fvalida.edad.focus()
      return 0;
    } else {
      if (edad < 18) {
        alert("Debe ser mayor de 18 años.")
        document.fvalida.edad.focus()
        return 0;
      }
    }

    if (document.fvalida.interes.selectedIndex == 0) {
      alert("Debe seleccionar un motivo de su contacto.")
      document.fvalida.interes.focus()
      return 0;
    }

    alert("Muchas gracias por enviar el formulario");
    document.fvalida.submit();
  }