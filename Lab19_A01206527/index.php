<?php
    session_start();
    require_once("util.php");
    include("_header.html");
    include("_agregar.html");
    echo "<h1>Todos los usuarios</h1>";
    echo getUsuario();
    echo "<h1>Telefonos de cada usuario</h1>";
    echo getNombreTel();
    echo "<h1>Usuarios con mas de 18 años</h1>";
    echo getEdad();
    
    echo "<h1>Preguntas</h1>";
    echo "<h3>Explica y elabora un diagrama sobre cómo funciona AJAX con jQuery.</h3>";
    echo ' <img src="ajax.jpg" /> ';
    echo "<ul>En el diagrama se puede ver gráficamente que el usuario puede interactuar únicamente con ajax sin necesidad de hacer peticiones al servidor. Esto a veces puede ser eficiente por que las peticiones se ejecutan más rápido, pero las validaciones son más seguras si llegan hasta el servidor.</ul>";
    echo "<ul>  </ul>";
    echo "<h3>¿Qué alternativas a jQuery existen?</h3>";
    echo "<ul>Backbone JS, React JS, Node JS, Angular JS, Sprint JS, Zepto JS</ul>";

    include("_footer.html");
    include("_footer.html");
    if (isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        include("_mensaje.html");
        unset($_SESSION["mensaje"]);
    }
      
?>