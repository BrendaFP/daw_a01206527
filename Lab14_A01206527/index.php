<?php
    include("util.php");
    echo "<h1>Todos los usuarios</h1>";
    echo getUsuario();
    echo "<h1>Telefonos de cada usuario</h1>";
    echo getNombreTel();
    echo "<h1>Usuarios con mas de 18 años</h1>";
    echo getEdad();
    echo "<h1>Preguntas</h1>";
    echo "<h3>¿Qué es ODBC y para qué es útil?</h3>";
    echo "<ul>Es un estándar de acceso a bases de datos que utilizan los sistemas Microsoft. Las siglas significan Open DataBase Connectivity. A través de ODBC, en un sistema Windows se puede conectar con cualquier base de datos. </ul>";
    echo "<h3>¿Qué es SQL Injection?</h3>";
    echo "<ul>SQL Injection es una vulnerabilidad que permite a un atacante realizar consultas a una base de datos, se vale de un incorrecto filtrado de la información que se pasa a través de los campos y/o variables que usa un sitio web, es por lo general usada para extraer credenciales y realizar accesos ilegítimos, práctica un tanto neófita, ya que un fallo de este tipo puede llegar a permitir ejecución de comandos en el servidor, subida y lectura de archivos, o peor aún, la alteración total de los datos almacenados.</ul>";
    echo "<h3>¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?</h3>";
    echo "<ul>a)Escapar los caracteres especiales utilizados en las consultas SQL</ul>";
    echo "<ul>b)Delimitar los valores de las consultas</ul>";
    echo "<ul>c)Verificar siempre los datos que introduce el usuario</ul>";
    echo "<ul>d)Asignar mínimos privilegios al usuario que conectará con la base de datos</ul>";
?>