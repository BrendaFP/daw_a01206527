<?php
    function connectDB(){
      $servername= "localhost";
      $username = "root";
      $password = "root";
      $dbname = "usuarios";
    
      $mysql = mysqli_connect($servername, $username, $password, $dbname);
    
      // Check connection
      if (!$mysql) {
          die("Connection failed: " . mysqli_connect_error());
      }
      return $mysql;
    }
    
    function closeDB($mysql){
      mysqli_close($mysql);
    
    }

    function getUsuario() {
        $db = connectDb();
        $query='SELECT * FROM usuarios';
         // Query execution; returns identifier of the result group
        $result = $db->query($query);
        $cards = "";
         // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
         $cards .= '
                <p>'.$fila["Nombre"].'</p>
                <p>'.$fila["Edad"].'<p>
                <p>'.$fila["Telefono"].'<p>
                <a href="#">'.$fila["Mail"].'</a><br>   
                <p>--------------------------------------<p>
               ';
    
        }
        mysqli_free_result($result);
        closeDb($db);
        return $cards;
    }
    
    function getEdad(){
        $db = connectDb();
        $sql = "SELECT Nombre, Edad, Telefono, Mail FROM usuarios WHERE Edad > 18";
        $result = $db->query($sql);
        $table = "
        <table border = '2px'>
            <thread>
                <tr>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Telefono</th>
                    <th>Correo</th>
    
                </tr>
            </thread>
            <tbody>";
    
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
    
            $table .= '
              <tr>
                  <td>'.$row["Nombre"].'</td>
                  <td>'.$row["Edad"].'</td>
                  <td>'.$row["Telefono"].'</td>
                  <td>'.$row["Mail"].'</td>
              </tr>';
        }
    
        mysqli_free_result($result);
        closeDb($db);
        $table .= "</tbody></table>";
        return $table;
    }

    function getNombreTel(){
        $db = connectDb();
        $sql = "SELECT Nombre,Telefono FROM usuarios ";
        $result = $db->query($sql);
        $table = "
        <table border = '2px'>
            <thread>
                <tr>
                    <th>Nombre</th>
                    <th>Telefono</th>
                </tr>
            </thread>
            <tbody>";
    
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
    
            $table .= '
              <tr>
                  <td>'.$row["Nombre"].'</td>
                 
                  <td>'.$row["Telefono"].'</td>
               
              </tr>';
        }
    
        mysqli_free_result($result);
        closeDb($db);
        $table .= "</tbody></table>";
        return $table;
    }

 
?>