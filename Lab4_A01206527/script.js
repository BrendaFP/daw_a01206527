function fun1() {
    var num = prompt('Ingresa un número');
    var div = document.getElementById('res1');
    div.innerHTML = "";

    var tabla = document.createElement("tabla");
    var tbody = document.createElement("tbody");

    var row = document.createElement("tr");

    var th = document.createElement("th");
    var thTexto = document.createTextNode("Número");
    th.appendChild(thTexto);
    row.appendChild(th);

    var th = document.createElement("th");
    var thTexto = document.createTextNode("Cuadrado");
    th.appendChild(thTexto);
    row.appendChild(th);

    var th = document.createElement("th");
    var thTexto = document.createTextNode("Cubo");
    th.appendChild(thTexto);
    row.appendChild(th);

    tbody.appendChild(row);

    for (var i = 1; i <= num; i++) {
        row = document.createElement("tr");
        for (var j = 1; j <= 3; j++) {
            var cell = document.createElement("td");
            var cell_text = document.createTextNode(Math.pow(i, j));
            cell.appendChild(cell_text);
            row.appendChild(cell);
        }
        tbody.appendChild(row);
    }
    tabla.appendChild(tbody);
    div.appendChild(tabla);
}

function fun2() {
    let a = Math.floor(Math.random()*50 +1);
    let b = Math.floor(Math.random()*50 +1);
    
    var start = window.performance.now();
    var answ = prompt("¿Cuánto es " + a + " + " + b + " ? ");
    var end = window.performance.now();
    var time = (end - start) / 1000;
     let res=document.createElement('span');

    if (a + b == answ) {
        alert("¡Correcto! Tardaste " + time.toFixed(2) + " segundos en contestar.");
    } else {
        alert("¡Incorrecto! La respuesta correcta es: " + (a + b) + ". Tardaste " + time.toFixed(2) + " segundos en contestar.")
    }
}

function fun3(array){
    var div = document.getElementById('res3');
    
    var contPos=0;
    var contNeg=0;
    var contCero=0;
    var arreglo="";
    
     for (i = 0; i < array.length; i++) {
        if (array[i] < 0) {
            contNeg++;
        } else if (array[i] == 0) {
            contCero++;
        } else {
            contPos++;
        }
         arreglo += " " + array[i]+",";
    }
    
    alert("El arreglo ["+ arreglo+"] tiene: \n"+"Negativos: "+contNeg+"\n"+"Poitivos: "+contPos+"\n"+"Ceros: "+contCero);
}

function fun4(){
    var div = document.getElementById('res4');
    var matriz=[[4,4,4,4,4],[2,6,4,9,3],[4,7,2,3,1],[1,2,3,4,5]];
    
    var res = " ";
    var mat = "\n Matriz: \n \n[[4,4,4,4,4],[2,6,4,9,3],[4,7,2,3,1],[1,2,3,4,5]] ";
    for (i = 0; i < matriz.length; i++) {
        var acum = 0;
        for (j = 0; j < matriz[i].length; j++) {
            acum = acum+ matriz[i][j];
        }
        var promedio = acum / (matriz[i].length);
        res += "<br>";
        res += "Promedio renglon " + i + ": \t" + promedio;
    }
    div.innerHTML = mat + res;
}

function fun5(num){
    var div = document.getElementById('res5');
    
    let cadena = num.toString() ;
    alert( "Cadena: "+num+"\n Cadena Invertida: "+cadena.split("").reverse().join(""));
}

function fun6(){
    var hora =new Date();
    var horas= hora.getHours().toString();
    var minutos = hora.getMinutes().toString();
    if(minutos.length==1){
        minutos="0"+minutos;
    }
    var segundos=hora.getSeconds().toString();
    if(segundos.length==1){
        segundos="0"+segundos;
    }
    document.forms[0].reloj.value=horas+ " : "+minutos+" : "+segundos
}

var boton1 = document.getElementById('fun1');
boton1.onclick = function () {
    fun1();
};

var boton2 = document.getElementById('fun2');
boton2.onclick = function(){
    fun2();
};

var boton3 = document.getElementById('fun3');
boton3.onclick = function(){
    var array;
    array=[-1,1,3,5,0,0,6,3,-6,-20,0,12,25];
    fun3(array);
};

var boton4 = document.getElementById('fun4');
boton4.onclick = function(){
    fun4();
};

var boton5 = document.getElementById('fun5');
boton5.onclick = function(){
    var num=123456789;
    fun5(num);
};

var boton6 = document.getElementById('fun6');
boton6.onclick = function(){
    var r=setInterval("fun6()",500);
};
