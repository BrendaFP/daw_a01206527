<!DOCTYPE html>
<html>
	<head>
        <title>Laboratorio </title>
        <meta charset="UTF-8">
	</head>
	<body>
        <p>
          <?php
            echo '<strong>Funcion 1: </strong>Una función que reciba un arreglo de números y devuelva su promedio<br><br>';
            function promedio($array){
                $prom = array_sum($array)/count($array);
                return $prom;
            }
            
            $array1 = [12,35,24,10,35];
            $array2 = [10,24,30,23,11];
            echo 'A= [12,35,24,10,35]<br>';
            $prom1=promedio($array1);
            echo 'Promedio A: ' .$prom1 . '<br/><br>';
            echo 'B = [10,24,30,23,11]<br>';
            $prom2=promedio($array2);
            echo 'Promedio B: ' .$prom2 . '<br/><br>';
            
            
            echo '<br><br><strong>Funcion 2: </strong>Una función que reciba un arreglo de números y devuelva su mediana<br><br>';
            function mediana($array3){
                sort($array3);
                $cont = count($array3);
                $mediana = ($cont + 1) / 2;
                return $array3[$mediana -1];
            }
            
            $array3=[10,24,25,234,21,68,82,1,68,21];
            echo "C= " . implode(", ", $array3) . "<br />";
            $med=mediana($array3);
            echo 'Mediana: ' .$med . '<br>';
            
            
            echo '<br><br><strong>Funcion 3: </strong>Una función que reciba un arreglo de números y muestre la lista de números, y como ítems de una lista html muestre el promedio, la mediana, y el arreglo ordenado de menor a mayor, y posteriormente de mayor a menor<br><br>';
            function lista($array4){
                echo "lista = " . implode(", ", $array4) . "<br />";
                $prom=promedio($array4);
                echo "<ul><li> Promedio: $prom </li></ul>";
                $med=mediana($array4);
                echo "<ul><li> Mediana: $med </li></ul>";
                sort($array4);
                echo "<ul><li>Menor a mayor = ". implode(", ", $array4)."</ul></li>";
                rsort($array4);
                echo "<ul><li>Mayor a menor = ". implode(", ", $array4)."</ul></li>";
            }
            $array4=[24,74,20,25,2,62,1,2,15];
            lista($array4);
            
             echo '<br><strong>Funcion 4: </strong> Una función que imprima una tabla html, que muestre los cuadrados y cubos de 1 hasta un número n<br><br>';
            function tabla_potencias($n) {
                $tabla .= "<table>";
                $tabla .= "<thead>";
                $tabla .= "<tr>";
                $tabla .= "<th>Número</th><th>Cuadrado</th><th>Cubo  </th>";
                $tabla .= "</tr>";
                $tabla .= "</thead><tbody>";

                for ($i = 1; $i <= $n; $i++) {
                    $tabla .= "<tr>";
                    $tabla .= "<td>$i</td><td>".$i*$i."</td><td>".$i*$i*$i."</td>";
                    $tabla .= "</tr>";
                }

                $tabla .= "</tbody></table>";

                return $tabla;
            }
            $tabla = tabla_potencias(10);
            echo $tabla;
            
            echo '<br><br><strong>Funcion 5: </strong> Concatenar un "+" entre las letras de una cadenas<br><br> ';
            function concatenar($cadena){ 
                for ($i=0;$i<strlen($cadena);$i++){ 
                    echo $cadena[$i]; 
                    if ($i<strlen($cadena)-1) 
                        echo "+"; 
                } 
            } 
            concatenar("hola"); 
            echo "<br>"; 
            concatenar ("El texto esta concatenado"); 
            
            echo '<br><br><br><strong>Cuestionario</strong>';
            echo '<br><br><li>¿Qué hace la función phpinfo()?</li>';
            echo '<br>La funcion phpinfo(), nos muestra información, sobre el sistema operativo que se esta utilizando, el servidor apache y version con la cual se esta trabajando y toda la información sobre PHP';
            echo '<br><br><li>¿Qué cambios tendrías que hacer en la configuración del servidor para que pudiera ser apto en un ambiente de producción?</li>';
            echo '<br>Se debe modificar el php.ini. Algunas configuraciones que se pueden hacer es: el register_globals: off, safe_mode: off, implicit_flush:off, entre otras. ';
            echo '<br><br><li>¿Cómo es que si el código está en un archivo con código html que se despliega del lado del cliente, se ejecuta del lado del servidor?</li>';
            echo '<br>Se despliega porque lo que se ejecuta es un archivo php que devuelve un archivo html al cliente.';
            
            
            echo '<br><br><strong>Referencias</strong>';
            echo '<br><ul><li>http://php.net/manual/en/function.phpinfo.php</li></ul>';
            echo '<ul><li>http://blog.unreal4u.com/2010/08/configuracion-de-php-testing-vs-produccion-vs-hosting/</li></ul>';
          ?>
        </p>   
	</body>
</html>

