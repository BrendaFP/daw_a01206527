document.addEventListener('click', function (e) {
  if (e.target.classList.contains('help_button')) {
    document.getElementById(e.target.getAttribute('data-content')).hidden = false;
    document.body.classList.add('shaded');
    return;
  }
  
  if (e.target.nodeName.toLowerCase() != 'a') {
    [].slice.call(document.querySelectorAll('.help_content'))
      .forEach(function (content) {
        content.hidden = true;
      });
    document.body.classList.remove('shaded');
  }
});

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
}

var time;
var n=0;
function reset() {
    n = 0;
    var l = document.getElementById("number");
    time=window.setInterval(function(){
        l.innerHTML = n;
        n++;
    },1000);
}

function start(){
    var l = document.getElementById("number");
    time=window.setInterval(function(){
        l.innerHTML = n;
        n++;
    },1000);
}
function stop() {
    clearInterval(time);
}
